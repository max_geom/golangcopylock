package db

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"log"
)

var schema = `
DROP TABLE IF EXISTS user;
CREATE TABLE user (
    id INTEGER PRIMARY KEY,
    email VARCHAR(255) DEFAULT '',
	code VARCHAR(255) DEFAULT '',
	host_id VARCHAR(255) DEFAULT '',
	status	INTEGER DEFAULT 0
);`

// User - структура пользователя в базе данных
type User struct {
	ID        int    `db:"id"`
	FirstName string `db:"first_name"`
	LastName  string `db:"last_name"`
	Email     string `db:"email"`
	Code      string `db:"code"`
	HostId    string `db:"host_id"`
	Status    int    `db:"status"`
}

// Connect - соеднение с базой
func Connect() *sqlx.DB {
	db, err := sqlx.Connect("sqlite3", "./sqlite3.db")
	if err != nil {
		log.Fatal(err)
	}
	return db
}

// StartLoad - функция для наката таблицы в бд
func StartLoad(db *sqlx.DB) {
	db.MustExec(schema)
}
