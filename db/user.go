package db

import (
	mail "../email"
	"crypto/sha1"
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"log"
	"time"
)

// makeTimestamp - функция уникальных значений по времени
func makeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

// generateCode - герерация кода для пользователя
func generateCode(email string) string {
	resultString := string(makeTimestamp())
	resultString += email
	hash := sha1.New()
	hash.Write([]byte(resultString))
	code := hash.Sum(nil)
	return base64.URLEncoding.EncodeToString(code)
}


// getUserByEmail - проверка наличия пользователя в базе данных
func getUserByEmail(db *sqlx.DB, email string) error {
	user := User{}
	err := db.Get(&user, "SELECT * FROM user WHERE email=$1", email)
	if err != nil {
		return errors.New("Не найдено")
	}
	return nil
}


// getUserByCode - получение пользователя по коду активации
func getUserByCode(db *sqlx.DB, code string) (User, error)  {
	user := User{}
	err := db.Get(&user, "SELECT * FROM user WHERE code=$1", code)
	if err != nil {
		log.Println("Пользователь не найден")
		return User{}, errors.New("Пользователь не найден")
	}
	return user, nil
}

// createNewUser - создание записи о новом пользоателе в базе данных
func createNewUser(db *sqlx.DB, email, code string)  {
	db.MustExec("INSERT INTO user (email, code) VALUES ($1, $2)", email, code)
}

// updateUserData - обновление пользовательских данных при активации программы пользователя
func updateUserData(db *sqlx.DB, user User)  {
	_, err := db.NamedExec("UPDATE user SET host_id = :host_id, status = 1 WHERE code = :code", user)
	if err != nil {
		fmt.Println(err)
	}
}

// CreateUser - функция обработки полученных данных
func CreateUser(email string) (bool, string) {
	if email == "" {
		return false, "Почта не задана"
	}
	con := Connect()
	defer con.Close()
	err := getUserByEmail(con, email)
	if err != nil {
		code := generateCode(email)
		go createNewUser(con, email, code)
		go mail.SendMail(email, code)
		return true, ""
	}
	return false, "Пользователю уже отправлены данные"
}

// ActivateRequest - структура запроса от пользователя
type ActivateRequest struct{
	Code string
	Hash string
}

// ActivateUserProgram - проверка и активации пользователя по его host_id
func ActivateUserProgram(requestBody ActivateRequest) (bool, string) {
	con := Connect()
	defer con.Close()
	user, err := getUserByCode(con, requestBody.Code)
	if err != nil {
		return false, "Пользователь не найден"
	}
	if user.Status == 1 {
		return false, "Программа уже активирован"
	}
	user.HostId = requestBody.Hash
	fmt.Println(user)
	go updateUserData(con, user)
	go mail.SendMail(user.Email, "Программа активирована")
	return true, "Программа активирована"
}

// CheckActivateProgram - проверка активации программы
func CheckActivateProgram(hash string) (status bool, text string) {
	con := Connect()
	defer con.Close()
	user := User{}
	err := con.Get(&user, "SELECT * FROM user WHERE host_id=$1", hash)
	fmt.Println(user)
	if err != nil {
		return false, err.Error()
	}
	return true, "Активировано"
}