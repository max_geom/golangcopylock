package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/shirou/gopsutil/host"
	"net/http"
	"os"
)

const (
	Url string = "http://localhost:8000/register"
)

// ResponseData - ответ от сервера
type ResponseData struct {
	StatusCode bool   `json:"status_code"`
	Text       string `json:"text"`
}

// checkCopyProgram - проверка программы на доступ
func checkCopyProgram(code string) ResponseData {
	netData, err := host.Info()
	if err != nil {
		fmt.Println("Код не найден")
		return ResponseData{StatusCode: false, Text: ""}
	}
	var response ResponseData

	requestBody, err := json.Marshal(map[string]string{
		"code": code,
		"hash": netData.HostID,
	})

	if err != nil {
		fmt.Println(err)
	}

	responseBody, err := http.Post(Url, "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		fmt.Println("Нет соединения с интерентом")
		os.Exit(1)
	}
	defer responseBody.Body.Close()

	json.NewDecoder(responseBody.Body).Decode(&response)

	return response
}


/*
	Программа, в которой применена защита от копирования на основе сервера
 */
func main() {
	var code string = ""
	var countBadTry int
	for {
		check := checkCopyProgram(code)
		if check.StatusCode {
			// Body Program
			fmt.Println("hello World")
			return
		} else {
			// Input code
			if countBadTry < 3 {
				fmt.Print("Введите код активации: ")
				fmt.Scan(&code)
				countBadTry += 1
			}else{
				fmt.Println("Вы испробовали все попытки!")
				os.Exit(0)
			}
		}
	}
}
