package main

import (
	"./db"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
)

// Response - структура ответа от сервера
type Response struct {
	StatusCode bool		`json:"status_code"`
	Text       string	`json:"text"`
}

// activeProgram - функция отправки запроса на сервер для получения активации или проверки подлинности программы
func activeProgram(request *http.Request) (response Response) {
	var result db.ActivateRequest
	json.NewDecoder(request.Body).Decode(&result)
	fmt.Println(result)
	response.StatusCode, response.Text = db.CheckActivateProgram(result.Hash)
	if response.StatusCode == false {
		response.StatusCode, response.Text = db.ActivateUserProgram(result)
		return
	}
	return
}

func init() {
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		tmpl := template.Must(template.ParseFiles("templates/index.html"))
		if request.Method != http.MethodPost {
			tmpl.Execute(writer, nil)
			return
		}

		status, err := db.CreateUser(request.FormValue("email"))
		tmpl.Execute(writer, struct {
			Success bool
			Error   string
		}{status, err})
	})

	http.HandleFunc("/register", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Content-Type", "application/json")
		if request.Method == http.MethodPost {
			response := activeProgram(request)
			json.NewEncoder(writer).Encode(response)
		}
	})
}

// dafc7237-397b-466b-a67a-2a4a70c9ebdc

func main() {
	argsServer := os.Args[1:]
	if len(argsServer) == 1 && argsServer[1] == "migrate" {
		db.StartLoad(db.Connect())
		return
	}

	log.Println("Севрер запушен")
	log.Fatal(http.ListenAndServe(":8000", nil))
}
